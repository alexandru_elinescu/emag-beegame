<?php session_start();
use App\Controllers\GameController as Game;

include_once '../vendor/autoload.php';

$load_game = new Game();
$load_game->load();