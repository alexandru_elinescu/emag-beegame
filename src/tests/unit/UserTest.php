<?php
namespace App\Tests;

use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    public function testCanCreatedUsername()
    {
        $user = new \App\Service\User;
        $user->setUsername('Alexandru');

        $this->assertEquals($user->getUsername(), 'Alexandru');
    }

}