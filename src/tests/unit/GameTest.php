<?php
namespace App\Tests;

use PHPUnit\Framework\TestCase;

final class GameTest extends TestCase
{
    protected $bees;

    public function setBees()
    {
        $this->bees = json_decode(json_encode([
            ['name' => 'Test Bee', 'health' => 20, 'type' => 'worker'],
            ['name' => 'Test 2 Bee', 'health' => 30, 'type' => 'worker']
        ]), false);
    }

    public function testCanReturnHitBee()
    {
        $this->setBees();
    
        $gamePlay = new \App\Business\GamePlay('play');
        $bee = $gamePlay->hitBee($this->bees);

        $this->assertCount(1, (array)$bee);
    }

    public function testCanHitBee()
    {
        $this->setBees();

        $gamePlay = new \App\Business\GamePlay('play', true);
        $bee = $gamePlay->attack($this->bees);

        $this->assertArrayHasKey('bees', $bee);
        $this->assertArrayHasKey('hitBee', $bee);
    }

}
