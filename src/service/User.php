<?php
namespace App\Service;

class User {
    protected $username;

    public function __construct(){
        $this->username = (isset($_COOKIE["username"]) && $_COOKIE["username"] != '') ? $_COOKIE["username"] : '';
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function removeUsername()
    {
        unset($_COOKIE['username']); 
        setCookie('username', null, -1, '/');
    }
    
    public function storeUsername()
    {
        setCookie("username", $this->username);
    }

    public function setCookie($name, $value = "",  $expire = 0,  $path = "", $domain = "", $secure = false, $httponly = false) {
        return setcookie($name, $value,  $expire, $path, $domain, $secure, $httponly);
    }
}