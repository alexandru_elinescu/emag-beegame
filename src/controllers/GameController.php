<?php
namespace App\Controllers;

use App\Core\View;
use App\Repositories\BeeRepository;

use App\Service\User;

class GameController extends View {
    protected $username;
    private $beeRepo;

    public function __construct(){
        $this->beeRepo = new BeeRepository();
    }

    public function load()
    {
        $user = new User();
        $this->username = $user->getUsername();
        if(strlen($this->username) > 2){
            $this->playGame();
        } else {
            $this->login($user);
        }
    }
    
    public function login($user)
    {
        if($_POST){
            $username = $_POST['username'];
            if(strlen($username) > 2){
                $username = $user->setUsername($username);
                $user->storeUsername();

                $this->username = $username;
                return $this->playGame();
            }
        }

        $this->render('home/login');
    }

    public function playGame()
    {
        $controls = isset($_GET['controls']) ? $_GET['controls'] : '';
        $bees = $this->beeRepo->getAll($controls);
        
        $this->render('home/game', [
                'game' => $bees,
                'username' => $this->username,
                'controls' => $controls
            ]
        );
    }

}