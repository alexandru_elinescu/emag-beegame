<?php
namespace App\Business;

class GamePlay {
    private $controls;
    private $tests;

    public function __construct($controls, $tests = false){
        $this->controls = $controls;
        $this->tests = $tests;
    }

    public function attack($bees)
    {       
        $hitBee = 0;
        $swarmHealth = 0;

        if($this->tests){
            $beeGame = $bees; //
        } else {
            $beeGame = $this->loadGame($bees); //
        }
       
        if($this->controls == 'restart'){
            $beeGame = $this->resetGame($bees);
        }

        $swarmHealth = $this->swarmHealth($beeGame);

        if($swarmHealth && $this->controls == 'hit'){
            $hitBee = $this->hitBee($beeGame);

            // calculam
            $damage = ($beeGame[$hitBee]->health - $beeGame[$hitBee]->damage);
            if($damage <= 0){
                $beeGame[$hitBee]->health = 0;
            } else {
                $beeGame[$hitBee]->health = $damage;
            }

            $swarmHealth = ($swarmHealth - $beeGame[$hitBee]->damage); //
            $swarmHealth = $this->swarmHealth($beeGame);
            
            // salvam
            if(!$this->tests){
                $this->saveGame($beeGame);
            }
        }

        return [
            'bees' => $beeGame,
            'hitBee' => $beeGame[$hitBee],
            'swarmHealth' => $swarmHealth,
            'swarmHealthPercentage' => $this->getPercentage($this->swarmTotalHealth($bees), $swarmHealth)
        ];
    }

    public function hitBee($bees){
        $hitBee = array_rand($bees, 1);
        if($bees[$hitBee]->health <= 0){
            return $this->hitBee($bees);
        } else {
            return $hitBee;
        }
    }

    public function swarmHealth($swarm)
    {
        $health = 0;
        foreach ($swarm as $bee) {
            if($bee->type == 'queen' && $bee->health <= 0){
                return 0;
            } else {
                $health += $bee->health;
            }
        }
        return $health;
    }

    public function swarmTotalHealth($swarm)
    {
        $health = 0;
        foreach ($swarm as $bee) {
            $health += $bee->health;
        }
        return $health;
    }

    function getPercentage($total, $number)
    {
        if ($total > 0) {
            return round($number / ($total / 100), 1);
        } else {
            return 0;
        }
    }





    //
    public function loadGame($bees)
    {
        if(isset($_COOKIE["gameData"]) && $_COOKIE["gameData"] != ''){
            return json_decode($_COOKIE["gameData"]);
        } else {
           return $this->saveGame($bees);
        }
    }
    public function saveGame($bees)
    {
        setcookie("gameData", json_encode($bees));
        return $bees;
    }
    public function resetGame($bees)
    {
        return $this->saveGame($bees);
    }
}