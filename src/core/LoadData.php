<?php
namespace App\Core;

class LoadData {
    private $file;

    public function __construct($file)
    {
        $this->file = $file;
    }
    public function getJson()
    {
        $strJsonFileContents = file_get_contents($this->file);
        $this->json = json_decode($strJsonFileContents);
        return $this;
    }
}