<?php
namespace App\Core;

abstract class Model {
    protected $dataPath = '../src/data';
    protected $get;
    
    public function __construct()
    {
       $this->loadData();
    }

    private function loadData()
    {
        $strJsonFileContents = file_get_contents($this->dataPath . '/'. $this->json . '.json');
        $this->get = json_decode($strJsonFileContents);
        return $this;
    }

    //
    public function get()
    {
        return $this->get;
    }

    function groupBy($key) {
        $result = [];
        foreach($this->get() as $val) {
            if(array_key_exists($key, $val)){
                $result[$val->{$key}][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
        return $result;
    }

}