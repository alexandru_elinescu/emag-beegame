<?php
namespace App\Core;

abstract class View {

    private function protect($x = null)
    {
        if (!isset($x))
            return null;
        elseif (is_string($x))
            return htmlspecialchars($x, ENT_QUOTES);
        elseif (is_array($x))
        {
            foreach($x as $k => $v)
            {
                $x[$k] = $this->protect($v);
            }
            return $x;
        }
        else
            return $x;
    }
    public function render($view, $data = [])
    {
        $data = $this->protect($data);

        if(file_exists('../src/views/' . $view . '.php')){
            require_once '../src/views/' . $view . '.php';
        }
    }
    public function error()
    {
     
        
    }
}