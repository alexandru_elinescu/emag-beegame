<?php
namespace App\Repositories;
use App\Model\Bee;
use App\Business\GamePlay;

class BeeRepository {

    public function getAll($controls)
    {       
        $model = new Bee();
        $bees = $model->get();

        $gamePlay = new GamePlay($controls);
        $remainingBees = $gamePlay->attack($bees);

        return [
            'bees' => $this->groupBy($remainingBees['bees'], 'type'),
            'hitBee' => $remainingBees['hitBee'],
            'swarmHealth' => $remainingBees['swarmHealth'],
            'swarmHealthPercentage' => $remainingBees['swarmHealthPercentage'],
        ];
    }

    public function getByType()
    {
        $model = new Bee();
        $bees = $model->groupBy('type');
        return $bees;
    }

    function groupBy($object, $key) {
        $result = [];
        foreach($object as $val) {
            if(array_key_exists($key, $val)){
                $result[$val->{$key}][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        return $result;
    }
}