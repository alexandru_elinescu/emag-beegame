<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>eMAG's Bees</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C400i%2C600%2C700&#038;ver=1.1.2" type="text/css" media="all" />
</head>
<body>
<div class="container">
    <br>
    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control">
            <small class="form-text text-muted">Minim 3 caractere.</small>
        </div>
        <button type="submit" class="btn btn-primary">SET USERNAME</button>
    </form>
    <br>
</div>
</body>
</html>
