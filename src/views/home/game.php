<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>eMAG's Bees</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C400i%2C600%2C700&#038;ver=1.1.2" type="text/css" media="all" />
</head>
<body>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div align="center" class="card-header">
                    <h4>BEES</h4>
                    <span class="badge badge-secondary">Swarm health: <?php echo $data['game']['swarmHealthPercentage'] ?> % (<?php echo $data['game']['swarmHealth']?> HP)</span>
                </div>
                <ul class="list-group">
                    <?php 
                    $bees = $data['game']['bees'];
                    foreach ($bees as $type => $beeTypes){?>
                        <li class="list-group-item">
                            <?php echo '<b>'. strtoupper($type) .'</b>';?>
                            <span class="badge badge-secondary"><?php echo count($beeTypes); ?></span>
                            <div>
                                <?php foreach ($beeTypes as $bee){?>
                                    <div>
                                        <?php echo $bee->name ?>
                                        <span class="badge <?php echo $bee->health ? 'badge-success': 'badge-danger' ?> pull-right mr-2">Health: <?php echo $bee->health ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <h2>Salut <?php echo $data['username']?></h2>
            <div class="card">
                <div class="card-body">
                    <?php if($data['game']['swarmHealth']){?>
                        <?php if($data['controls'] == 'hit'){?>
                            <div><b><?php echo $data['game']['hitBee']->name ?></b> he was hit with a power of <?php echo $data['game']['hitBee']->damage ?></div>
                        <?php } else {?>
                            <div>Press the `<strong>Hit</strong>` button to attack a bee :)</div>
                        <?php }?>
                    <?php } else {?>
                        <b>GAME OVER</b>
                    <?php }?>
                </div>
            </div>
    
            <br>
            <?php if($data['game']['swarmHealth']){?>
                <a href="?controls=hit" class="btn btn-danger"><strong>HIT</strong></a>
            <?php } else {?>
                <a href="?controls=restart"" class="btn btn-primary"><strong> <i class="fa fa-play"></i> &nbsp; PLAY AGAIN</strong></a>
            <?php }?>
        </div>
    </div>
    <br>
</div>
</body>
</html>
